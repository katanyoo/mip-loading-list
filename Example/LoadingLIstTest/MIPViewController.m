//
//  MIPViewController.m
//  LoadingLIstTest
//
//  Created by Katanyoo Ubalee on 2/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MIPViewController.h"
#import "MIPLoadingList.h"

@implementation MIPViewController

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - Methods

- (IBAction)startDownload:(id)sender
{
    /*
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"FileList" ofType:@"plist"];
    NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:filePath];
    NSArray *fileList = [[dict objectForKey:@"files"] copy];
    */
    
    
    NSURL *url = [NSURL URLWithString:@"http://www.myiphone.in.th/MIPLL/FileList.plist"];
    
    MIPLoadingList *loader = [[MIPLoadingList alloc] init];
    [loader startDownloadWithSourceURL:url];
}


#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSLog(@"test");
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

@end
