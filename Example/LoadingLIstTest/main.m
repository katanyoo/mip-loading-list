//
//  main.m
//  LoadingLIstTest
//
//  Created by Katanyoo Ubalee on 2/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MIPAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MIPAppDelegate class]));
    }
}
