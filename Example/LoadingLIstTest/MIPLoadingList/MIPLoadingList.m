//
//  MIPLoadingList.m
//  LoadingLIstTest
//
//  Created by Katanyoo Ubalee on 3/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MIPLoadingList.h"
#import "ASIHTTPRequest.h"
#import "ASINetworkQueue.h"

@implementation MIPLoadingList

- (NSString *)cachesDirectory
{
    NSArray *pathList = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    return [pathList  objectAtIndex:0];
}

- (void)updateProgress
{
    float p = ((float)([networkQueue bytesDownloadedSoFar]*1.0)/([networkQueue totalBytesToDownload]*1.0))*100;
    
    if (p > 0) {
        progress.text = [NSString stringWithFormat:@"%.0f%%", p];
        
    }
    
    if ((int)p < 100) {
        [self performSelector:@selector(updateProgress) withObject:nil afterDelay:0.2];
    }
}

- (void)hideIndicator
{
    [UIView animateWithDuration:0.2 
                     animations:^{
                         busyBG.alpha = 0;
                     }
                     completion:^(BOOL finished){
                         [busyBG removeFromSuperview];
                     }];
}

- (void)showIndicator
{
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    
    busyBG = [[UIView alloc] initWithFrame:keyWindow.frame];
    busyBG.backgroundColor = [UIColor colorWithWhite:0 alpha:0.2];
    
    UIView *popup = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 150, 150)];
    popup.alpha = 0.0f;
    popup.center = busyBG.center;
    popup.backgroundColor = [UIColor colorWithWhite:0.8 alpha:1];
    
    popup.layer.cornerRadius = 10.0f;
    popup.layer.shadowColor = [[UIColor blackColor] CGColor];
    popup.layer.shadowRadius = 10.0f;
    popup.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    popup.layer.shadowOpacity = 0.5f;

    UILabel *waitLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 150, 50)];
    waitLabel.text = @"Downloading";
    waitLabel.textAlignment = UITextAlignmentCenter;
    waitLabel.textColor = [UIColor darkGrayColor];
    waitLabel.backgroundColor = [UIColor clearColor];
    waitLabel.layer.shadowOffset = CGSizeMake(0, -1);
    [popup addSubview:waitLabel];
    
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    indicator.hidesWhenStopped = YES;
    indicator.frame = CGRectMake(150/2 - 37/2, 50, 37, 37);
    [indicator startAnimating];
    [popup addSubview:indicator];
    
    progress = [[UILabel alloc] initWithFrame:CGRectMake(0, 87, 150, 63)];
    progress.text = @"0%";
    progress.textAlignment = UITextAlignmentCenter;
    progress.font = [UIFont boldSystemFontOfSize:40];
    progress.textColor = [UIColor darkGrayColor];
    progress.backgroundColor = [UIColor clearColor];
    progress.layer.shadowOffset = CGSizeMake(0, -1);
    [popup addSubview:progress];
    
    [busyBG addSubview:popup];
    
    [keyWindow addSubview:busyBG];
    
    [UIView animateWithDuration:0.2 animations:^{
        popup.alpha = 1.0f;
    }];
    
    [self performSelector:@selector(updateProgress) withObject:nil afterDelay:0.2];
    
}

- (void)startDownloadWithFiles:(NSArray *)files
{
    if (!networkQueue) {
		networkQueue = [[ASINetworkQueue alloc] init];	
	}

	[networkQueue reset];
	[networkQueue setRequestDidFinishSelector:@selector(downloadComplete:)];
	[networkQueue setRequestDidFailSelector:@selector(downloadFailed:)];
	[networkQueue setShowAccurateProgress:YES];
	[networkQueue setDelegate:self];
    
    requestCount = [files count];
    
    for (NSString *item in files) {
        NSString *fileName = [item lastPathComponent];
        
        ASIHTTPRequest *request;
        request = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:item]];
        [request setDownloadDestinationPath:[[self cachesDirectory] stringByAppendingPathComponent:fileName]];
        [networkQueue addOperation:request];
    }
    
    [networkQueue go];
    [self showIndicator];
}

- (void)downloadComplete:(ASIHTTPRequest *)request
{
    --requestCount;
    if (requestCount == 0) {
        [self hideIndicator];
    }
}

- (void)downloadFailed:(ASIHTTPRequest *)request
{
    NSLog(@"download fail");
}

- (void)startDownloadWithSourceURL:(NSURL *)url
{
    ASIHTTPRequest *request;
    request = [ASIHTTPRequest requestWithURL:url];
    [request setDelegate:self];
    [request setDownloadDestinationPath:[[self cachesDirectory] stringByAppendingPathComponent:@"data.mipd"]];
    [request startAsynchronous];
    
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *dataPath = [[self cachesDirectory] stringByAppendingPathComponent:@"data.mipd"];
    
    if ([fm fileExistsAtPath:dataPath]) {
        NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:dataPath];
        NSArray *fileList = [[dict objectForKey:@"files"] copy];
        [self startDownloadWithFiles:fileList];
    }
    
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    //NSError *error = [request error];
}

- (void)dealloc {

	[networkQueue reset];
	[networkQueue release];
    [super dealloc];
}

@end
