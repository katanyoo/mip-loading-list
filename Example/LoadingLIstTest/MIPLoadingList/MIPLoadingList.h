//
//  MIPLoadingList.h
//  LoadingLIstTest
//
//  Created by Katanyoo Ubalee on 3/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>

@class ASINetworkQueue;

@interface MIPLoadingList : NSObject
{
    ASINetworkQueue *networkQueue;
    UILabel *progress;
    int requestCount;
    UIView *busyBG;
}

- (void)startDownloadWithFiles:(NSArray *)files;
- (void)startDownloadWithSourceURL:(NSURL *)url;
//- (void)showIndicator;

@end
