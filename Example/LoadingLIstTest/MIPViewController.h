//
//  MIPViewController.h
//  LoadingLIstTest
//
//  Created by Katanyoo Ubalee on 2/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MIPViewController : UIViewController

- (IBAction)startDownload:(id)sender;

@end
