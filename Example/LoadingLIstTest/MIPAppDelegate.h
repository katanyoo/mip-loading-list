//
//  MIPAppDelegate.h
//  LoadingLIstTest
//
//  Created by Katanyoo Ubalee on 2/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MIPViewController;

@interface MIPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) MIPViewController *viewController;

@end
